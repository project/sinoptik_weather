<?php

namespace Drupal\sinoptik_weather\Plugin\Block;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\sinoptik_weather\Controller\CityAutocompleteController;

/**
 * Creates Sinoptik.ua Weather Block.
 *
 * @Block(
 *   id = "sinoptik_weather_informer",
 *   admin_label = @Translation("Sinoptik.ua Weather Informer"),
 *   category = @Translation("Block"),
 * )
 */
class SinoptikWeatherBlock extends BlockBase {

  const REQUEST_GET_SPELLED_CITIES = 'https://sinoptik.ua/%D0%B8%D0%BD%D1%84%D0%BE%D1%80%D0%BC%D0%B5%D1%80%D1%8B';
  const SINOPTIK_WEBSITE_URL_UA = 'https://ua.sinoptik.ua';
  const SINOPTIK_WEBSITE_URL = 'https://sinoptik.ua';
  const SINOPTIK_10_DAYS_UA = '10-%D0%B4%D0%BD%D1%96%D0%B2';
  const SINOPTIK_10_DAYS = '10-%D0%B4%D0%BD%D0%B5%D0%B9';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'lang' => '',
      'cities' => [],
      'width' => '',
      'color' => 'type1c1',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    static $key = 0;
    $result = [];
    // Only 1 instance of block on a page is currently supported by sinoptik.
    // TODO: Find a workaround for this.
    if ($key < 1) {
      $result['#theme'] = 'sinoptik_weather';
      foreach ($this->getConfiguration() as $key => $value) {
        $result['#' . $key] = $value;
      }

      $lang_is_ua = 'ua' == $this->configuration['lang'];
      $ten_days = $lang_is_ua ? self::SINOPTIK_10_DAYS_UA : self::SINOPTIK_10_DAYS;
      $url = $lang_is_ua ? self::SINOPTIK_WEBSITE_URL_UA : self::SINOPTIK_WEBSITE_URL;
      $sinoptik_url = $url . '/';
      if (1 === count($result['#cities'])) {
        $sinoptik_url .= reset($result['#cities'])['path'] . '/';
      }

      $result['#url'] = $url;
      $result['#sinoptik_link'] = Link::fromTextAndUrl('sinoptik.ua', Url::fromUri($sinoptik_url . $ten_days))->toString();

    }

    $result['#attached']['library'][] = 'sinoptik_weather/sinoptik';
    $result['#attached']['drupalSettings']['sinoptikWeather']['cities'] = array_column($this->configuration['cities'], 'id');
    $result['#attached']['drupalSettings']['sinoptikWeather']['lang'] = $this->configuration['lang'];
    $result['#cache']['max-age'] = 0;

    $key++;
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $langcode = $this->getDefaultLangcode($form_state);

    $form['lang'] = [
      '#title' => $this->t('Widget Language'),
      '#type' => 'select',
      '#options' => $this->getSupportedLangOptions(),
      '#default_value' => $this->configuration['lang'] ?: $langcode,
      '#ajax' => [
        'callback' => [$this, 'languageCallback'],
        'wrapper' => 'sinoptik-city-wrapper',
      ],
    ];
    $form['city_wrapper'] = ['#type' => 'container', '#id' => 'sinoptik-city-wrapper'];
    $form['city_wrapper']['cities'] = [
      '#title' => $this->t('City'),
      '#type' => 'textfield',
      '#autocomplete_route_name' => 'sinoptik_weather.autocomplete.cities',
      '#autocomplete_route_parameters' => ['field_name' => 'city', 'lang' => $langcode],
      '#tags' => TRUE,
      '#default_value' => $this->prepareCitiesString(),
      '#description' => $this->t('Start typing city name and then select location from dropdown.'),
      '#required' => TRUE,
    ];

    $form['width'] = [
      '#title' => $this->t('Informer Width'),
      '#description' => $this->t('Leave empty to fit wrapper width.'),
      '#type' => 'number',
      '#min' => 160,
      '#max' => 350,
      '#default_value' => $this->configuration['width'],
      '#field_suffix' => 'px',
    ];
    $form['color'] = [
      '#title' => $this->t('Informer Major Color'),
      '#type' => 'radios',
      '#options' => [
        'type1c1' => '<span style="background: #ecf3fa; width: 14px; height: 14px; border: 1px solid #a3b4c8;">#ecf3fa</span>',
        'type1' => '<span data-color="fff" style="background: #ffffff; width: 14px; height: 14px; border: 1px solid #a3b4c8;">#ffffff</span>',
      ],
      '#default_value' => $this->configuration['color'],
    ];

    return $form;
  }

  /**
   * Gets default lang code.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   *
   * @return string
   *   Compatible with Sinoptik language code (ua, ru or en).
   */
  protected function getDefaultLangcode(FormStateInterface $form_state) {
    $user_input = $form_state->getUserInput();
    if (isset($user_input['settings']['lang'])) {
      return $user_input['settings']['lang'];
    }
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    return !in_array($language, ['ru', 'en']) ? 'ua' : $language;
  }

  /**
   * Gets the list of supported languages.
   *
   * @return \Drupal\Core\StringTranslation\TranslationInterface[]
   *   The list of supported languages where keys are Sinoptik language codes.
   */
  protected function getSupportedLangOptions() {
    return [
      'ua' => $this->t('Ukrainian'),
      'ru' => $this->t('Russian'),
      'en' => $this->t('English'),
      'pl' => $this->t('Polish'),
    ];
  }

  /**
   * Ajax callback to translate cities field if lang is changed.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   *
   * @return array
   *   City form element array with translated cities in value.
   */
  public function languageCallback(array &$form, FormStateInterface $form_state) {
    $lang = $form_state->getUserInput()['settings']['lang'];
    $form['settings']['city_wrapper']['cities']['#autocomplete_route_parameters']['lang'] = $lang;
    $current_cities = $this->parseCitiesString($form['settings']['city_wrapper']['cities']['#value']);
    $cities_data = $this->getCitiesData(array_keys($current_cities), $lang);
    $form['settings']['city_wrapper']['cities']['#value'] = $this->prepareCitiesString($cities_data);
    return $form['settings']['city_wrapper'];
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    $cities_value = $form_state->getValue(['city_wrapper', 'cities']);

    $cities = $this->parseCitiesString($cities_value);
    if (empty($cities) && !empty($cities_value)) {
      $form_state->setErrorByName('city_wrapper][cities', $this->t('Wrong format of city.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    // Process the block's submission handling if no errors occurred only.
    if (!$form_state->getErrors()) {
      $lang = $form_state->getValue('lang');
      $cities = $this->parseCitiesString($form_state->getValue(['city_wrapper', 'cities']));

      $this->setConfigurationValue('lang', $lang);
      $this->setConfigurationValue('cities', $this->getCitiesData(array_keys($cities), $lang));
      $this->setConfigurationValue('width', $form_state->getValue('width'));
      $this->setConfigurationValue('color', $form_state->getValue('color'));
    }
  }

  /**
   * Parse Cities String.
   *
   * @param string $cities
   *   String with cities to parse.
   *
   * @return array
   *   The list of cites where keys are city IDs and values are city names.
   */
  protected function parseCitiesString($cities) {
    $result = [];
    $list_of_cities = explode(',', $cities);
    foreach ($list_of_cities as $city_string) {
      $city = trim($city_string);
      $matches = [];
      preg_match('/^([^\(]+)(?:\s+\([^\)]+\))?\s+\(\s+([0-9]+)\s+\)/', $city, $matches);
      if (!empty($matches)) {
        $result[$matches[2]] = $matches[1];
      }
    }
    return $result;
  }

  /**
   * Converts array of cities to string.
   *
   * @param array $cities
   *   An array of cities in format returned by $this->getCitiesData().
   *
   * @return string
   *   List of cities, separated by comma in tag format: "City Name ( id )".
   */
  protected function prepareCitiesString(array $cities = []) {
    $cities_list = [];
    foreach (($cities ?: $this->configuration['cities']) as $city_data) {
      $cities_list[] = CityAutocompleteController::formatAutocompleteValue($city_data['name'], $city_data['id']);
    }
    return implode(', ', $cities_list);
  }

  /**
   * Gets information about cities in appropriate language.
   *
   * @param int[] $city_ids
   *   The list of city ids.
   * @param string $lang
   *   Language. Possible values: 'ua', 'ru', 'en'.
   *
   * @return array
   *   A multidimentional array of found cities, where keys are city IDs and
   *   values are arrays with the following key-values:
   *    - id - city sinoptik ua unique ID
   *    - name - The city name.
   *    - path - path to the city to build the URL.
   *    - weather_in_city - translated version of string "Weather in :city:".
   */
  protected function getCitiesData(array $city_ids = [], $lang = NULL) {
    $cities = $city_ids ?: array_keys($this->configuration['cities']);
    $http_client_options = ['query' => ['ajax' => 'GetSettle']];
    $http_client_options['query']['cities'] = implode(',', $cities);
    $http_client_options['query']['lang'] = $lang ?: $this->configuration['lang'];

    $results = [];
    $response = \Drupal::httpClient()->request('GET', self::REQUEST_GET_SPELLED_CITIES, $http_client_options);
    if (200 !== $response->getStatusCode()) {
      return $results;
    }
    $contents = $response->getBody()->getContents();
    foreach (Json::decode($contents) as $line) {
      list($name, $path, $city_id, $city_spelled, $weather_in_city) = str_getcsv($line, '|');
      if (empty($weather_in_city)) {
        $weather_in_city = $this->t('Weather @city', ['@city' => $city_spelled]);
      }
      $results[$city_id] = [
        'id' => $city_id,
        'name' => $name,
        'path' => $path,
        'weather_in_city' => $weather_in_city,
      ];
    }
    return $results;
  }

}
