<?php

namespace Drupal\sinoptik_weather\Controller;

use Drupal\Core\Controller\ControllerBase;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CityAutocompleteController.
 */
class CityAutocompleteController extends ControllerBase {

  const SINOPTIK_URL = 'https://sinoptik.ua';

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a new CityAutocompleteController object.
   */
  public function __construct(ClientInterface $http_client) {
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client')
    );
  }

  /**
   * Handleautocomplete.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object.
   * @param string $field_name
   *   City name.
   * @param string $lang
   *   Language code. Possible values: en, ru, ua.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Json response with the list of cities.
   */
  public function handleAutocomplete(Request $request, $field_name = '', $lang = 'ua') {
    $results = [];

    if ('city' != $field_name) {
      return new JsonResponse($results);
    }

    $http_client_options = ['query' => ['informers' => '1']];
    $http_client_options['query']['q'] = $request->get('q');
    $http_client_options['query']['lang'] = $lang;

    $response = $this->httpClient->request('GET', self::SINOPTIK_URL . '/search.php', $http_client_options);

    if (200 == $response->getStatusCode()) {
      $csv = $response->getBody()->getContents();
      foreach (explode("\n", $csv) as $line) {
        list($city, $description, , $city_id) = str_getcsv($line, '|');
        $results[] = [
          'value' => $this->formatAutocompleteValue($city, $city_id),
          'label' => $this->t('@city - @description', [
            '@city' => $city,
            '@description' => $description,
          ]),
        ];
      }
    }
    return new JsonResponse($results);
  }

  /**
   * Prepares string for autocomplete widget.
   *
   * @param string $city
   *   City name.
   * @param int $id
   *   Sinoptik.ua city unique ID.
   *
   * @return \Drupal\Core\StringTranslation\TranslationInterface
   *   Translated string in format "City ( ID )".
   */
  public static function formatAutocompleteValue($city, $id) {
    return t('@city ( @id )', [
      '@city' => $city,
      '@id' => $id,
    ]);
  }

}
