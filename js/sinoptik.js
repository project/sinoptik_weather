/**
 * @file
 * Sinoptik Weather informer JS loader.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Loads sinoptik.ua weather informer script.
   */
  Drupal.behaviors.sinoptikWeatherScript = {
    attach: function (context, settings) {
      $('.SinoptikInformer', context).closest('body').once('sinoptikWeather').each(function() {
        let sinoptikScript = document.createElement('script');
        sinoptikScript.src = '//sinoptik.ua/informers_js.php?title=4&wind=3&cities='
          + settings.sinoptikWeather.cities.join() + '&lang=' + settings.sinoptikWeather.lang;
        $(this).append(sinoptikScript);
      });
    }
  };

}(jQuery, Drupal));
